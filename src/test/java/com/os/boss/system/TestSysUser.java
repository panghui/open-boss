package com.os.boss.system;

import com.os.boss.system.dao.SysUserDao;
import com.os.boss.system.entitys.SysUser;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/7/18
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring_config/ApplicationContext.xml",
        "classpath:/spring_config/spring-mybatis.xml","classpath:/spring_config/spring-security.xml",
        "classpath:/spring_config/spring-utils.xml"})
public class TestSysUser {

    public static final Logger logger = Logger.getLogger(TestSysUser.class);


    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private Md5PasswordEncoder md5PasswordEncoder;

    @Test
    public void testQueryById(){
        SysUser sysUser = sysUserDao.selectByPrimaryKey(1L);
        logger.info("登录名："+sysUser.getLoginName());
        logger.info("真是姓名："+sysUser.getName());
    }


    @Test
    public void testMd5Password(){
        String password = "123456";
        String password2 = "f57d9fe2fe8feceabebbc43607cba112";
        String salt = "admin";
        logger.info("加密前密码："+password);
        logger.info("加密后密码："+md5PasswordEncoder.encodePassword(password,salt));
        logger.info("解密密码：" + md5PasswordEncoder.isPasswordValid(password2,password,salt));
    }

}
