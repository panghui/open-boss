/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : open_boss

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-08-12 13:49:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `class_method` varchar(100) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `operate_user` varchar(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-04 11:04:23');
INSERT INTO `sys_log` VALUES ('2', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-05 11:04:48');
INSERT INTO `sys_log` VALUES ('3', '更改用户状态，返回成功与否信息', 'com.os.boss.system.controller.SysUserAction.updateSysUserStatus()', '127.0.0.1', 'admin', '2016-08-05 11:04:59');
INSERT INTO `sys_log` VALUES ('4', '更改用户状态，返回成功与否信息', 'com.os.boss.system.controller.SysUserAction.updateSysUserStatus()', '127.0.0.1', 'admin', '2016-08-06 11:05:13');
INSERT INTO `sys_log` VALUES ('5', '更改用户状态，返回成功与否信息', 'com.os.boss.system.controller.SysUserAction.updateSysUserStatus()', '127.0.0.1', 'admin', '2016-08-07 11:05:13');
INSERT INTO `sys_log` VALUES ('6', '更改用户状态，返回成功与否信息', 'com.os.boss.system.controller.SysUserAction.updateSysUserStatus()', '127.0.0.1', 'admin', '2016-08-09 11:05:19');
INSERT INTO `sys_log` VALUES ('7', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '127.0.0.1', 'admin', '2016-08-09 11:05:30');
INSERT INTO `sys_log` VALUES ('8', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-09 11:07:09');
INSERT INTO `sys_log` VALUES ('9', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-09 11:07:11');
INSERT INTO `sys_log` VALUES ('10', '更改用户状态，返回成功与否信息', 'com.os.boss.system.controller.SysUserAction.updateSysUserStatus()', '127.0.0.1', 'admin', '2016-08-09 11:07:17');
INSERT INTO `sys_log` VALUES ('11', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-09 11:07:26');
INSERT INTO `sys_log` VALUES ('12', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-09 11:07:32');
INSERT INTO `sys_log` VALUES ('13', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '127.0.0.1', 'admin', '2016-08-09 11:07:53');
INSERT INTO `sys_log` VALUES ('14', '获取用户列表信息', 'com.os.boss.system.controller.SysUserAction.getUserList()', '127.0.0.1', 'admin', '2016-08-09 11:07:55');
INSERT INTO `sys_log` VALUES ('15', '更新资源信息', 'com.os.boss.system.controller.PermissionAction.updateResource()', '127.0.0.1', 'admin', '2016-08-09 13:59:27');
INSERT INTO `sys_log` VALUES ('16', '更新资源信息', 'com.os.boss.system.controller.PermissionAction.updateResource()', '127.0.0.1', 'admin', '2016-08-09 14:06:31');
INSERT INTO `sys_log` VALUES ('17', '添加用户', 'com.os.boss.system.controller.SysUserAction.addUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-10 17:47:59');
INSERT INTO `sys_log` VALUES ('18', '修改用户信息', 'com.os.boss.system.controller.SysUserAction.updateSysUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-10 17:50:20');
INSERT INTO `sys_log` VALUES ('19', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 11:13:31');
INSERT INTO `sys_log` VALUES ('20', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 11:18:20');
INSERT INTO `sys_log` VALUES ('21', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 11:23:47');
INSERT INTO `sys_log` VALUES ('22', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 11:31:02');
INSERT INTO `sys_log` VALUES ('23', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 11:33:14');
INSERT INTO `sys_log` VALUES ('24', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 11:38:36');
INSERT INTO `sys_log` VALUES ('25', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 13:29:47');
INSERT INTO `sys_log` VALUES ('26', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 13:30:53');
INSERT INTO `sys_log` VALUES ('27', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 13:33:23');
INSERT INTO `sys_log` VALUES ('28', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 13:34:47');
INSERT INTO `sys_log` VALUES ('29', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '127.0.0.1', 'admin', '2016-08-11 13:35:44');
INSERT INTO `sys_log` VALUES ('30', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 13:43:03');
INSERT INTO `sys_log` VALUES ('31', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 13:45:56');
INSERT INTO `sys_log` VALUES ('32', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 14:01:34');
INSERT INTO `sys_log` VALUES ('33', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 14:04:20');
INSERT INTO `sys_log` VALUES ('34', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 14:04:52');
INSERT INTO `sys_log` VALUES ('35', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 14:05:55');
INSERT INTO `sys_log` VALUES ('36', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 16:29:46');
INSERT INTO `sys_log` VALUES ('37', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 16:31:31');
INSERT INTO `sys_log` VALUES ('38', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:06:10');
INSERT INTO `sys_log` VALUES ('39', '删除角色', 'com.os.boss.system.controller.SysRoleAction.delRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:06:34');
INSERT INTO `sys_log` VALUES ('40', '删除角色', 'com.os.boss.system.controller.SysRoleAction.delRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:07:46');
INSERT INTO `sys_log` VALUES ('41', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:08:39');
INSERT INTO `sys_log` VALUES ('42', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:08:51');
INSERT INTO `sys_log` VALUES ('43', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:10:47');
INSERT INTO `sys_log` VALUES ('44', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:11:50');
INSERT INTO `sys_log` VALUES ('45', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:12:42');
INSERT INTO `sys_log` VALUES ('46', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:14:31');
INSERT INTO `sys_log` VALUES ('47', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:53:37');
INSERT INTO `sys_log` VALUES ('48', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:53:45');
INSERT INTO `sys_log` VALUES ('49', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:54:33');
INSERT INTO `sys_log` VALUES ('50', '更新角色', 'com.os.boss.system.controller.SysRoleAction.updateRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:54:40');
INSERT INTO `sys_log` VALUES ('51', '添加角色', 'com.os.boss.system.controller.SysRoleAction.addRole()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:55:28');
INSERT INTO `sys_log` VALUES ('52', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:55:57');
INSERT INTO `sys_log` VALUES ('53', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:56:03');
INSERT INTO `sys_log` VALUES ('54', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:56:09');
INSERT INTO `sys_log` VALUES ('55', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:56:14');
INSERT INTO `sys_log` VALUES ('56', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:56:21');
INSERT INTO `sys_log` VALUES ('57', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:56:30');
INSERT INTO `sys_log` VALUES ('58', '添加用户', 'com.os.boss.system.controller.SysUserAction.addUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:57:48');
INSERT INTO `sys_log` VALUES ('59', '添加用户', 'com.os.boss.system.controller.SysUserAction.addUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:58:58');
INSERT INTO `sys_log` VALUES ('60', '添加用户', 'com.os.boss.system.controller.SysUserAction.addUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 17:59:36');
INSERT INTO `sys_log` VALUES ('61', '根据用户ID，删除用户数据', 'com.os.boss.system.controller.SysUserAction.deleteUser()', '0:0:0:0:0:0:0:1', 'admin', '2016-08-11 18:00:13');

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource` (
  `res_id` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `res_pid` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `level` bigint(20) DEFAULT NULL,
  `isleaf` int(11) DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `creator` varchar(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modifier` varchar(20) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES ('01', '0', null, '1', '0', '管理员管理', '&#xe62d;', '1', null, '2016-08-05 16:52:47', null, null);
INSERT INTO `sys_resource` VALUES ('0101', '01', 'role/showRole', '2', '1', '角色管理', null, '1', null, '2016-08-05 16:54:02', null, null);
INSERT INTO `sys_resource` VALUES ('0102', '01', 'permission/showPermission', '2', '1', '权限管理', null, '1', null, '2016-08-05 16:54:40', null, null);
INSERT INTO `sys_resource` VALUES ('0103', '01', 'sysUser/getUserList', '2', '1', '管理员管理', null, '1', null, '2016-08-05 16:55:38', null, null);
INSERT INTO `sys_resource` VALUES ('02', '0', null, '1', '0', '系统管理', '&#xe62e;', '1', null, '2016-08-05 16:56:16', null, null);
INSERT INTO `sys_resource` VALUES ('0201', '02', 'log/showLogList', '2', '1', '系统日志', null, '1', null, '2016-08-05 16:57:03', null, '2016-08-09 14:06:31');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `creator` varchar(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modifier` varchar(20) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '管理员，可以操作所有节点', '1', null, '2016-08-01 14:47:34', 'admin', '2016-08-11 17:06:09');
INSERT INTO `sys_role` VALUES ('36', '管理员', '拥有管理员管理角色', '1', 'admin', '2016-08-11 17:14:31', 'admin', '2016-08-11 17:53:45');
INSERT INTO `sys_role` VALUES ('37', '系统管理员', '拥有系统资源下所有节点操作权限', '1', 'admin', '2016-08-11 17:54:33', 'admin', '2016-08-11 17:54:39');
INSERT INTO `sys_role` VALUES ('38', '用户管理员', '拥有管理员下的用户管理菜单资源', '1', 'admin', '2016-08-11 17:55:28', null, null);

-- ----------------------------
-- Table structure for sys_role_res
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_res`;
CREATE TABLE `sys_role_res` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `res_id` varchar(30) DEFAULT NULL,
  `creator` varchar(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modifier` varchar(20) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_role_res
-- ----------------------------
INSERT INTO `sys_role_res` VALUES ('27', '1', '01', null, null, 'admin', '2016-08-11 17:06:09');
INSERT INTO `sys_role_res` VALUES ('28', '1', '0101', null, null, 'admin', '2016-08-11 17:06:10');
INSERT INTO `sys_role_res` VALUES ('29', '1', '0102', null, null, 'admin', '2016-08-11 17:06:10');
INSERT INTO `sys_role_res` VALUES ('30', '1', '0103', null, null, 'admin', '2016-08-11 17:06:10');
INSERT INTO `sys_role_res` VALUES ('31', '1', '02', null, null, 'admin', '2016-08-11 17:06:10');
INSERT INTO `sys_role_res` VALUES ('32', '1', '0201', null, null, 'admin', '2016-08-11 17:06:10');
INSERT INTO `sys_role_res` VALUES ('41', '36', '01', 'admin', '2016-08-11 17:14:31', null, null);
INSERT INTO `sys_role_res` VALUES ('42', '36', '0101', 'admin', '2016-08-11 17:14:31', null, null);
INSERT INTO `sys_role_res` VALUES ('43', '36', '0102', 'admin', '2016-08-11 17:14:31', null, null);
INSERT INTO `sys_role_res` VALUES ('44', '36', '0103', 'admin', '2016-08-11 17:14:31', null, null);
INSERT INTO `sys_role_res` VALUES ('51', '37', '02', null, null, 'admin', '2016-08-11 17:54:39');
INSERT INTO `sys_role_res` VALUES ('52', '37', '0201', null, null, 'admin', '2016-08-11 17:54:39');
INSERT INTO `sys_role_res` VALUES ('53', '38', '01', 'admin', '2016-08-11 17:55:28', null, null);
INSERT INTO `sys_role_res` VALUES ('54', '38', '0103', 'admin', '2016-08-11 17:55:28', null, null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=763676228963139585 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'b594510740d2ac4261c1b2fe87850d08', '庞辉', '18601231223', 'panghui@datang.com', '1', '2016-07-18 23:01:07', '2016-07-18 23:01:10');
INSERT INTO `sys_user` VALUES ('763675777358233600', 'superAdmin', '46851c6ea74ab440fd33f347dffcca1e', '庞辉', '15810989987', 'superAdmin@admin.com', '1', '2016-08-11 17:57:48', null);
INSERT INTO `sys_user` VALUES ('763676072163278848', 'userAdmin', '2ca35623089b127d0ea6566284c63e8c', '郭海鹏', '15810989987', 'guohaipeng@12.com', '1', '2016-08-11 17:58:58', null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `creator` varchar(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modifier` varchar(20) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1', null, '2016-08-01 14:49:01', null, null);
INSERT INTO `sys_user_role` VALUES ('9', '763675777358233600', '1', 'admin', '2016-08-11 17:57:48', null, null);
INSERT INTO `sys_user_role` VALUES ('10', '763676072163278848', '38', 'admin', '2016-08-11 17:58:58', null, null);
