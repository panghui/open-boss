<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/8/3
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="http://lib.h-ui.net/html5.js"></script>
<script type="text/javascript" src="http://lib.h-ui.net/respond.min.js"></script>
<script type="text/javascript" src="http://lib.h-ui.net/PIE_IE678.js"></script>

  <![endif]-->
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/icheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/skin/default/skin.css" id="skin" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/style.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-page.css" />
  <link rel="stylesheet" href="${ctx}/css/ztree/zTreeDiv.css" type="text/css">
  <link rel="stylesheet" href="${ctx}/css/ztree/zTreeStyle.css" type="text/css">
  <!--[if IE 6]>
  <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>添加角色 - 管理员管理</title>
</head>
<body>
<article class="page-container">
  <form class="form form-horizontal" id="form-role-add" method="post">
    <input type="hidden" name="roleId" value="${sysRole.roleId}"/>
    <input type="hidden" name="resIds" id="resIds"/>
    <input type="hidden" name="isOperate" id="isOperate" value="${isOperate}"/>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色名称：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="${sysRole.roleName}" id="roleName" name="roleName" placeholder=""/>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3">角色描述：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <textarea name="description" cols="" rows="" class="textarea"  placeholder="说点什么...100个字符以内" dragonfly="true" onKeyUp="textarealength(this,100)">${fn:trim(sysRole.description)}</textarea>
        <p class="textarea-numberbar"><em class="textarea-length">0</em>/100</p>
      </div>
    </div>
    <div class="row c1">
      <label class="form-label col-xs-4 col-sm-3">选择资源：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <ul id="treeResource" class="ztree"></ul>
      </div>
    </div>
    <div class="row cl">
      <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
        <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;更新&nbsp;&nbsp;">
      </div>
    </div>
  </form>
</article>

<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.icheck.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/js/validate-methods.js"></script>
<script type="text/javascript" src="${ctx}/js/messages_zh.min.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${ctx}/js/ztree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="${ctx}/js/ztree/jquery.ztree.excheck.js"></script>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">

  // zTree 节点设置
  var setting = {
    check: {
      enable: true,
      chkStyle: "checkbox",
      chkboxType: { "Y" : "ps", "N" : "ps" }
    },
    data: {
      simpleData: {
        enable: true
      }
    },
    callback: {
      onCheck: getCheckNodes
    }
  };

  var zNodes = ${treeData};

  // 获取所有选中节点
  function getCheckNodes() {
    var treeObj = $.fn.zTree.getZTreeObj("treeResource");
    var nodes = treeObj.getCheckedNodes(true);
    var resource='';
    for(var i = 0; i < nodes.length; i++) {
      resource+=nodes[i].id+",";
    }
    $("#isOperate").val(1);
    $("#resIds").val(resource);
  }

  $(function(){

    $.fn.zTree.init($("#treeResource"), setting, zNodes);

    $('.skin-minimal input').iCheck({
      checkboxClass: 'icheckbox-blue',
      radioClass: 'iradio-blue',
      increaseArea: '20%'
    });

    $("#form-role-add").validate({
      rules:{
        roleName:{
          required:true,
          minlength:2,
          maxlength:10
        }
      },
      onkeyup:false,
      focusCleanup:true,
      success:"valid",
      submitHandler:function(form){
        $.ajax({
          type : "POST",
          url : "${ctx}/role/updateRole",
          data : $("#form-role-add").serialize(),
          success : function(data) {
            if(data == 1){
              layer.msg('更新角色成功!',{icon:1,time:1000});
              setTimeout(function () {
                var index = parent.layer.getFrameIndex(window.name);
                parent.location.replace(parent.location.href);
                parent.layer.close(index);
              }, 1000);
            }else{
              layer.msg('更新角色失败!',{icon:1,time:1000});
              setTimeout(function () {
                var index = parent.layer.getFrameIndex(window.name);
                parent.location.replace(parent.location.href);
                parent.layer.close(index);
              }, 1000);
            }
          }
        })
      }
    });
  });



</script>

</body>
</html>
