<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/7/26
  Time: 23:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <!--[if lt IE 9]>
  <script type="text/javascript" src="lib/html5.js"></script>
  <script type="text/javascript" src="lib/respond.min.js"></script>
  <script type="text/javascript" src="lib/PIE_IE678.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/icheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/skin/default/skin.css" id="skin" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/style.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-page.css" />
  <!--[if IE 6]>
  <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
  <![endif]-->
  <title>角色管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 管理员管理 <span class="c-gray en">&gt;</span> 角色管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
  <form id="sysRoleForm" method="post" action="${ctx}/role/showRole">
    <input type="hidden" id="pageNum" name="pageNum" value="1"/>
    <div class="text-l">
      角色名称：<input type="text" class="input-text" style="width:250px" placeholder="输入管理员登录名称" id="roleName" name="roleName" value="${condition.roleName}"/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <button type="submit" class="btn btn-success"><i class="Hui-iconfont">&#xe665;</i> 搜角色</button>
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <span class="l">
          <a href="javascript:;" onclick="datadel()" class="btn btn-danger radius">
            <i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>
          <a href="javascript:;" onclick="admin_role_add('添加角色','${ctx}/role/showAddRole','800','500')" class="btn btn-primary radius">
            <i class="Hui-iconfont">&#xe600;</i> 添加角色</a>
        </span>
      <span class="r">共有数据：<strong style="color: red;">${count}</strong> 条</span>
    </div>
  </form>
  <table class="table table-border table-bordered table-hover table-bg">
    <thead>
    <tr>
      <th scope="col" colspan="6">角色管理</th>
    </tr>
    <tr class="text-c">
      <th width="25"><input type="checkbox" value="" name=""></th>
      <th width="40">ID</th>
      <th width="200">角色名</th>
      <th width="300">描述</th>
      <th width="70">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageObject.list}" var="sysRole" varStatus="var">
      <tr class="text-c">
        <td><input type="checkbox" value="" name=""></td>
        <td>${var.index +1}</td>
        <td>${sysRole.roleName}</td>
        <td>${sysRole.description}</td>
        <td class="f-14">
          <a title="编辑" href="javascript:;" onclick="admin_role_edit('角色编辑','${ctx}/role/showUpdateRole?roleId='+${sysRole.roleId},'1')" style="text-decoration:none">
            <i class="Hui-iconfont">&#xe6df;</i></a>
          <a title="删除" href="javascript:;" onclick="admin_role_del(this,${sysRole.roleId})" class="ml-5" style="text-decoration:none">
            <i class="Hui-iconfont">&#xe6e2;</i></a></td>
      </tr>
    </c:forEach>
    </tbody>
  </table>
</div>
<!--显示页码标签-->
<div class="page_navigator" style="text-align: right;"></div>

<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctx}/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.page.js"></script>

<script type="text/javascript">

  $(".page_navigator").createPage({
    pageCount:${pageObject.totalPageCount},
    current:${pageObject.pageNum},
    backFn:function(p){
      $("#pageNum").val(p);
      searchSysRoleList();
    }
  });

  function searchSysRoleList(){
    $("#sysRoleForm").submit();
  }


  /*管理员-角色-添加*/
  function admin_role_add(title,url,w,h){
    layer_show(title,url,w,h);
  }
  /*管理员-角色-编辑*/
  function admin_role_edit(title,url,id,w,h){
    layer_show(title,url,w,h);
  }
  /*管理员-角色-删除*/
  function admin_role_del(obj,id){
    layer.confirm('角色删除须谨慎，确认要删除吗？',function(index){
      //此处请求后台程序，下方是成功后的前台处理……
      $.ajax({type: "get",url: "${ctx}/role/delRole?roleId="+id,success: function(data) {
        if(data!="0"){
          location.reload();
          layer.msg('已删除!',{icon:1,time:1000});
        }else{
          layer.msg('删除失败!',{icon: 5,time:1000});
        }
      }
      });
    });
  }
</script>
</body>
</html>
