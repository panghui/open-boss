<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/7/26
  Time: 23:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <!--[if lt IE 9]>
  <script type="text/javascript" src="lib/html5.js"></script>
  <script type="text/javascript" src="lib/respond.min.js"></script>
  <script type="text/javascript" src="lib/PIE_IE678.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/icheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/skin/default/skin.css" id="skin" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/style.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-page.css" />
  <!--[if IE 6]>
  <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
  <![endif]-->
  <title>管理员管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 管理员管理 <span class="c-gray en">&gt;</span> 管理员列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
  <form id="sysUserForm" method="post" action="${ctx}/sysUser/getUserList">
      <input type="hidden" id="pageNum" name="pageNum" value="1"/>
      <div class="text-l">
        登录名称：<input type="text" class="input-text" style="width:250px" placeholder="输入管理员登录名称" id="loginName" name="loginName" value="${condition.loginName}"/>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        姓名：<input type="text" class="input-text" style="width:250px" placeholder="输入姓名" id="name" name="name" value="${condition.name}"/>



        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电话：<input type="text" class="input-text" style="width:250px" placeholder="输入电话" id="tel" name="tel" value="${condition.tel}"/>
          <br/><br>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        邮箱：<input type="text" class="input-text" style="width:250px" placeholder="输入邮箱" id="email" name="email" value="${condition.email}"/>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button type="submit" class="btn btn-success"><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
      </div>
      <div class="cl pd-5 bg-1 bk-gray mt-20">
        <span class="l">
          <a href="javascript:;" onclick="datadel()" class="btn btn-danger radius">
            <i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>
          <a href="javascript:;" onclick="admin_add('添加管理员','${ctx}/sysUser/showAdminAdd','800','500')" class="btn btn-primary radius">
            <i class="Hui-iconfont">&#xe600;</i> 添加管理员</a>
        </span>
        <span class="r">共有数据：<strong style="color: red;">${count}</strong> 条</span>
      </div>
  </form>
  <table class="table table-border table-bordered table-hover table-bg">
    <thead>
    <tr>
      <th scope="col" colspan="9">员工列表</th>
    </tr>
    <tr class="text-c">
      <th width="25"><input type="checkbox" name="" value=""></th>
      <th width="40">ID</th>
      <th width="150">登录名</th>
      <th width="90">真实姓名</th>
      <th width="130">加入时间</th>
      <th width="100">是否已启用</th>
      <th width="100">操作</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${pageObject.list}" var="sysUser" varStatus="var">
    <tr class="text-c">
      <td><input type="checkbox" value="${sysUser.userId}" name=""></td>
      <td>${var.index+1}</td>
      <td>${sysUser.loginName}</td>
      <td>${sysUser.name}</td>
      <td><fmt:formatDate value="${sysUser.createTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
      <td class="td-status">
          <c:if test="${sysUser.status ==1}">
            <span class="label label-success radius">已启用</span>
          </c:if>
          <c:if test="${sysUser.status ==0}">
              <span class="label radius">已停用</span>
          </c:if>
      </td>
      <td class="td-manage">
        <!-- 已启用 -->
        <c:if test="${sysUser.status ==1}">
          <a style="text-decoration:none" onClick="admin_stop(this,'${sysUser.userId}')" href="javascript:;" title="停用">
            <i class="Hui-iconfont">&#xe631;</i></a>
        </c:if>
        <!-- 已停用 -->
        <c:if test="${sysUser.status ==0}">
          <a style="text-decoration:none" onClick="admin_start(this,'${sysUser.userId}')" href="javascript:;" title="启用">
            <i class="Hui-iconfont">&#xe631;</i></a>
        </c:if>
        <a title="编辑" href="javascript:;" onclick="admin_edit('管理员编辑','${ctx}/sysUser/showAdminEdit?userId='+'${sysUser.userId}','1','800','400')"
           class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a>
        <a title="删除" href="javascript:;" onclick="admin_del(this,'${sysUser.userId}')" class="ml-5" style="text-decoration:none">
          <i class="Hui-iconfont">&#xe6e2;</i></a>
      </td>
    </tr>
    </c:forEach>

    </tbody>
  </table>
</div>
<!--显示页码标签-->
<div class="page_navigator" style="text-align: right;"></div>

<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctx}/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.page.js"></script>
<script type="text/javascript">

    $(".page_navigator").createPage({
        pageCount:${pageObject.totalPageCount},
        current:${pageObject.pageNum},
        backFn:function(p){
            $("#pageNum").val(p);
            searchSysUserList();
        }
    });


    function searchSysUserList(){
        $("#sysUserForm").submit();
    }

  /*
   参数解释：
   title	标题
   url		请求的url
   id		需要操作的数据id
   w		弹出层宽度（缺省调默认值）
   h		弹出层高度（缺省调默认值）
   */
  /*管理员-增加*/
  function admin_add(title,url,w,h){
    layer_show(title,url,w,h);
  }
  /*管理员-删除*/
  function admin_del(obj,id){
    layer.confirm('确认要删除吗？',function(index){
      //此处请求后台程序，下方是成功后的前台处理……
      $.ajax({type: "get",url: "${ctx}/sysUser/deleteUser?userId="+id,success: function(data) {
          if(data=="1"){
            location.reload();
            layer.msg('已删除!',{icon:1,time:1000});
          }else{
            layer.msg('删除失败!',{icon: 5,time:1000});
          }
        }
      });
    });
  }
  /*管理员-编辑*/
  function admin_edit(title,url,id,w,h){
    layer_show(title,url,w,h);
  }
  /*管理员-停用*/
  function admin_stop(obj,id){
    layer.confirm('确认要停用吗？',function(index){
      //此处请求后台程序，下方是成功后的前台处理……

      $.ajax({type: "get",url: "${ctx}/sysUser/updateSysUserStatus?userId="+id,success: function(data){
        if(data=="1"){
          $(obj).parents("tr").find(".td-manage").prepend('<a onClick="admin_start(this,'+"'"+id+"'"+')" href="javascript:;" title="启用" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>');
          $(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">已停用</span>');
          $(obj).remove();
          layer.msg('已停用!',{icon: 5,time:1000});
        }else{
          layer.msg('停用失败!',{icon: 5,time:1000});
        }
      }
      });

    });
  }

  /*管理员-启用*/
  function admin_start(obj,id){
    layer.confirm('确认要启用吗？',function(index){
      //此处请求后台程序，下方是成功后的前台处理……

      $.ajax({type: "get",url: "${ctx}/sysUser/updateSysUserStatus?userId="+id,success: function(data){
        if(data=="1"){
          $(obj).parents("tr").find(".td-manage").prepend('<a onClick="admin_stop(this,'+"'"+id+"'"+')" href="javascript:;" title="停用" style="text-decoration:none"><i class="Hui-iconfont">&#xe631;</i></a>');
          $(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
          $(obj).remove();
          layer.msg('已启用!', {icon: 6,time:1000});
        }else{
          layer.msg('启用失败!',{icon: 5,time:1000});
        }
      }
      });

    });
  }
</script>
</body>
</html>
