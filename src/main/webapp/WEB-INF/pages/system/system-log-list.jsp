<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/7/26
  Time: 23:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <!--[if lt IE 9]>
  <script type="text/javascript" src="lib/html5.js"></script>
  <script type="text/javascript" src="lib/respond.min.js"></script>
  <script type="text/javascript" src="lib/PIE_IE678.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/icheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/skin/default/skin.css" id="skin" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/style.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-page.css" />
  <!--[if IE 6]>
  <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
  <![endif]-->
  <title>管理员管理</title>
</head>
<body>
<nav class="breadcrumb">
  <i class="Hui-iconfont">&#xe67f;</i> 首页
  <span class="c-gray en">&gt;</span> 系统管理
  <span class="c-gray en">&gt;</span> 系统日志列表
  <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
     href="javascript:location.replace(location.href);" title="刷新" >
    <i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="page-container">
  <form id="sysLogForm" method="post" action="${ctx}/log/showLogList">
      <input type="hidden" id="pageNum" name="pageNum" value="1"/>
      <div class="text-l">
        日期范围：
        <input type="text" value="${condition.startDateTime}" onclick="WdatePicker();" readonly id="startDateTime" name="startDateTime" class="input-text Wdate" style="width:120px; ">
        -
        <input type="text" value="${condition.endDateTime}" onclick="WdatePicker()" readonly id="endDateTime" name="endDateTime" class="input-text Wdate" style="width:120px;">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button type="submit" class="btn btn-success"><i class="Hui-iconfont">&#xe665;</i> 搜日志</button>
      </div>
      <div class="cl pd-5 bg-1 bk-gray mt-20">
        <span class="r">共有数据：<strong style="color: red;">${count}</strong> 条</span>
      </div>
  </form>
  <table class="table table-border table-bordered table-hover table-bg">
    <thead>
    <tr>
      <th scope="col" colspan="6">日志列表</th>
    </tr>
    <tr class="text-c">
      <th width="20">ID</th>
      <th width="150">方法描述</th>
      <th>操作类及方法</th>
      <th width="130">IP地址</th>
      <th width="100">操作人</th>
      <th width="120">时间</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${pageObject.list}" var="sysLog" varStatus="var">
    <tr class="text-c">
      <td>${var.index+1}</td>
      <td>${sysLog.description}</td>
      <td>${sysLog.classMethod}</td>
      <td>${sysLog.ip}</td>
      <td>${sysLog.operateUser}</td>
      <td><fmt:formatDate value="${sysLog.createTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
    </tr>
    </c:forEach>

    </tbody>
  </table>
</div>
<!--显示页码标签-->
<div class="page_navigator" style="text-align: right;"></div>

<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctx}/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.page.js"></script>
<script type="text/javascript">

    $(".page_navigator").createPage({
        pageCount:${pageObject.totalPageCount},
        current:${pageObject.pageNum},
        backFn:function(p){
            $("#pageNum").val(p);
          searchSysLogList();
        }
    });


    function searchSysLogList(){
        $("#sysLogForm").submit();
    }

</script>
</body>
</html>
