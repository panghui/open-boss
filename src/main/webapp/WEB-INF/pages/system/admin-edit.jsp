<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/8/3
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="http://lib.h-ui.net/html5.js"></script>
<script type="text/javascript" src="http://lib.h-ui.net/respond.min.js"></script>
<script type="text/javascript" src="http://lib.h-ui.net/PIE_IE678.js"></script>

  <![endif]-->
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/icheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/skin/default/skin.css" id="skin" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/style.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-page.css" />
  <!--[if IE 6]>
  <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>添加管理员 - 管理员管理</title>
</head>
<body>
<article class="page-container">
  <form class="form form-horizontal" id="form-admin-edit" method="post">
    <input type="hidden" name="userId" value="${sysUser.userId}"/>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>用户名：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="${sysUser.loginName}" placeholder="" id="loginName" name="loginName" disabled />
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>姓名：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="${sysUser.name}" placeholder="" id="name" name="name"/>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>手机：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="${sysUser.tel}" placeholder="" id="tel" name="tel"/>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>邮箱：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" placeholder="@" name="email" id="email" value="${sysUser.email}"/>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3">角色：</label>
      <div class="formControls col-xs-8 col-sm-9"> <span class="select-box" style="width:150px;">
			<select class="select" name="roleId" size="1">
              <c:forEach items="${listRole}" var="role">
                <option value="${role.roleId}" <c:if test="${role.roleId eq roleId}">selected="selected" </c:if>>${role.roleName}</option>
              </c:forEach>
            </select>
			</span> </div>
    </div>
    <div class="row cl">
      <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
        <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;修改&nbsp;&nbsp;">
      </div>
    </div>
  </form>
</article>

<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.icheck.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/js/validate-methods.js"></script>
<script type="text/javascript" src="${ctx}/js/messages_zh.min.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.admin.js"></script>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
  $(function(){
    $('.skin-minimal input').iCheck({
      checkboxClass: 'icheckbox-blue',
      radioClass: 'iradio-blue',
      increaseArea: '20%'
    });

    $("#form-admin-edit").validate({
      rules:{
        tel:{
          required:true,
          isPhone:true
        },
        email:{
          required:true,
          email:true
        },
        name:{
          required:true
        }
      },
      onkeyup:false,
      focusCleanup:true,
      success:"valid",
      submitHandler:function(form){
        $.ajax({
          type : "POST",
          url : "${ctx}/sysUser/updateSysUser",
          data : $("#form-admin-edit").serialize(),
          success : function(data) {
            if(data == 1){
              layer.msg('修改成功!',{icon:1,time:1000});
              setTimeout(function () {
                var index = parent.layer.getFrameIndex(window.name);
                parent.location.replace(parent.location.href);
                parent.layer.close(index);
              }, 1000);
            }else{
              layer.msg('修改失败!',{icon:1,time:1000});
              setTimeout(function () {
                var index = parent.layer.getFrameIndex(window.name);
                parent.location.replace(parent.location.href);
                parent.layer.close(index);
              }, 1000);
            }
          }
        })
      }
    });
  });



</script>

</body>
</html>
