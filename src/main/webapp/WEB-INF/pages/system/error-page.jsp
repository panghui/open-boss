<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/8/11
  Time: 11:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
    <title>错误页面</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
</head>
<body>
<section class="container-fluid page-404 minWP text-c">
    <p class="error-title"><i class="Hui-iconfont va-m" style="font-size:80px">&#xe688;</i><span class="va-m"> 发生错误</span></p>
    <p class="error-description">不好意思，${model.ex}</p>
    <p class="error-info">您可以：<a href="javascript:;" onclick="history.go(-1)" class="c-primary">&lt; 返回上一页</a><span class="ml-20">|</span><a href="${cxt}/showIndex" class="c-primary ml-20">去首页 &gt;</a></p>
</section>
</body>
</html>
