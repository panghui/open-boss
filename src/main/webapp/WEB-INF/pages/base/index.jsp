<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hadoop
  Date: 2016/7/26
  Time: 23:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<html>

<head>
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <LINK rel="Bookmark" href="/favicon.ico">
  <LINK rel="Shortcut Icon" href="/favicon.ico" />
  <!--[if lt IE 9]>
  <script type="text/javascript" src="lib/html5.js"></script>
  <script type="text/javascript" src="lib/respond.min.js"></script>
  <script type="text/javascript" src="lib/PIE_IE678.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.min.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/H-ui.admin.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/Hui-iconfont/1.0.7/iconfont.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/lib/icheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="${ctx}/skin/default/skin.css" id="skin" />
  <link rel="stylesheet" type="text/css" href="${ctx}/css/style.css" />
  <!--[if IE 6]>
  <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
  <script>DD_belatedPNG.fix('*');</script>
  <![endif]-->
  <title>开源通用后台</title>
  <meta name="keywords" content="H-ui.admin v2.4,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
  <meta name="description" content="H-ui.admin v2.4，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>

<body>
<header class="navbar-wrapper">
  <div class="navbar navbar-fixed-top">
    <div class="container-fluid cl">
      <a class="logo navbar-logo f-l mr-10 hidden-xs" href="/aboutHui.shtml">开源通用后台</a>
      <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/aboutHui.shtml">H-ui</a> <span class="logo navbar-slogan f-l mr-10 hidden-xs">v1.0</span>
      <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
      <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
        <ul class="cl">
          <li>${currentUserRole.roleName}</li>
          <li class="dropDown dropDown_hover">
            <a href="#" class="dropDown_A">${loginName} <i class="Hui-iconfont">&#xe6d5;</i></a>
            <ul class="dropDown-menu menu radius box-shadow">
              <li>
                <a href="#">个人信息</a>
              </li>
              <li>
                <a href="#">切换账户</a>
              </li>
              <li>
                <a href="${ctx}/logout">退出</a>
              </li>
            </ul>
          </li>
          <li id="Hui-msg">
            <a href="#" title="消息"><span class="badge badge-danger">1</span><i class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a>
          </li>
          <li id="Hui-skin" class="dropDown right dropDown_hover">
            <a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
            <ul class="dropDown-menu menu radius box-shadow">
              <li>
                <a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a>
              </li>
              <li>
                <a href="javascript:;" data-val="blue" title="蓝色">蓝色</a>
              </li>
              <li>
                <a href="javascript:;" data-val="green" title="绿色">绿色</a>
              </li>
              <li>
                <a href="javascript:;" data-val="red" title="红色">红色</a>
              </li>
              <li>
                <a href="javascript:;" data-val="yellow" title="黄色">黄色</a>
              </li>
              <li>
                <a href="javascript:;" data-val="orange" title="绿色">橙色</a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</header>
<aside class="Hui-aside">
  <input runat="server" id="divScrollValue" type="hidden" value="" />
  <div class="menu_dropdown bk_2">

    <c:forEach items="${listRes}" var="res">
      <dl id="menu-admin">
          <dt>
            <i class="Hui-iconfont">${res.icon}</i>
            ${res.name}<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
          <dd>
            <ul>
              <c:forEach items="${res.childResourceList}" var="childRes">
                <li>
                  <a _href="${childRes.url}" data-title="${childRes.name}" href="javascript:void(0)">${childRes.name}</a>
                </li>
              </c:forEach>
            </ul>
          </dd>
      </dl>
    </c:forEach>

    <%--<dl id="menu-admin">--%>
      <%--<dt><i class="Hui-iconfont">&#xe62d;</i> 管理员管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>--%>
      <%--<dd>--%>
        <%--<ul>--%>
          <%--<li>--%>
            <%--<a _href="role/showRole" data-title="角色管理" href="javascript:void(0)">角色管理</a>--%>
          <%--</li>--%>
          <%--<li>--%>
            <%--<a _href="permission/showPermission" data-title="权限管理" href="javascript:void(0)">权限管理</a>--%>
          <%--</li>--%>
          <%--<li>--%>
            <%--<a _href="sysUser/getUserList" data-title="管理员列表" href="javascript:void(0)">管理员列表</a>--%>
          <%--</li>--%>
        <%--</ul>--%>
      <%--</dd>--%>
    <%--</dl>--%>
    <%--<dl id="menu-system">--%>
      <%--<dt><i class="Hui-iconfont">&#xe62e;</i> 系统管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>--%>
      <%--<dd>--%>
        <%--<ul>--%>
          <%--<li>--%>
            <%--<a _href="system-base.html" data-title="系统设置" href="javascript:void(0)">系统设置</a>--%>
          <%--</li>--%>
          <%--<li>--%>
            <%--<a _href="system-category.html" data-title="栏目管理" href="javascript:void(0)">栏目管理</a>--%>
          <%--</li>--%>
          <%--<li>--%>
            <%--<a _href="system-data.html" data-title="数据字典" href="javascript:void(0)">数据字典</a>--%>
          <%--</li>--%>
          <%--<li>--%>
            <%--<a _href="system-shielding.html" data-title="屏蔽词" href="javascript:void(0)">屏蔽词</a>--%>
          <%--</li>--%>
          <%--<li>--%>
            <%--<a _href="system-log.html" data-title="系统日志" href="javascript:void(0)">系统日志</a>--%>
          <%--</li>--%>
        <%--</ul>--%>
      <%--</dd>--%>
    <%--</dl>--%>
  </div>
</aside>
<div class="dislpayArrow hidden-xs">
  <a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
</div>
<section class="Hui-article-box">
  <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
    <div class="Hui-tabNav-wp">
      <ul id="min_title_list" class="acrossTab cl">
        <li class="active"><span title="我的桌面" data-href="welcome.html">我的桌面</span><em></em></li>
      </ul>
    </div>
    <div class="Hui-tabNav-more btn-group">
      <a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a>
      <a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a>
    </div>
  </div>
  <div id="iframe_box" class="Hui-article">
    <div class="show_iframe">
      <div style="display:none" class="loading"></div>
      <iframe scrolling="yes" frameborder="0" src="${ctx}/welcome.html"></iframe>
    </div>
  </div>
</section>
<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.js"></script>
<script type="text/javascript" src="${ctx}/js/H-ui.admin.js"></script>


</body>

</html>
