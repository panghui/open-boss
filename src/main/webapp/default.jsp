<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript" src="${ctx}/lib/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
  $(function()  {
    window.location.href="${ctx}/showIndex";
  })
</script>