package com.os.boss.constant;

/**
 * 常量类
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/2
 */
public class Constants {

    // 登录错误
    public static String LOGIN_ERROR = "LOGIN_ERROR";

}
