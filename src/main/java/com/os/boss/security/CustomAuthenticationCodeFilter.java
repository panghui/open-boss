package com.os.boss.security;

import com.os.boss.exception.OBossException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义图片验证码校验类
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/1
 */
public class CustomAuthenticationCodeFilter extends UsernamePasswordAuthenticationFilter {

    Logger logger = LoggerFactory.getLogger(CustomAuthenticationCodeFilter.class);


    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response)
            throws AuthenticationException {

        String code = request.getParameter("code");

        String genCaptcha = (String)request.getSession().getAttribute("checkcode");

        logger.info("开始校验验证码，生成的验证码为："+genCaptcha+" ，输入的验证码为："+code);

        if( !genCaptcha.equals(code.toUpperCase())){
//            request.setAttribute("exceptionMsg",new OBossException("验证码错误"));
            throw new OBossException("验证码输入错误");
        }
        return super.attemptAuthentication(request, response);
    }

}
