package com.os.boss.security;

import com.os.boss.system.dao.SysRoleDao;
import com.os.boss.system.dao.SysUserDao;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.entitys.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Spring Security 通过用户名获取用户信息以及权限
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/1
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysRoleDao sysRoleDao;

    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        logger.info("登录账号：" + s);
        org.springframework.security.core.userdetails.User userDetails = null;
        SysUser user = sysUserDao.selectByLoginName(s);
        if (user == null) {
            throw new UsernameNotFoundException("用户名或密码不正确");
        }

        //账号密码错误，可以在这里手动抛出异常，让验证失败处理器AuthenticationFailureHandler进行处理

        Collection<GrantedAuthority> grantedAuthorities = getGrantedAuthorities(user);
        boolean enables = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        userDetails = new org.springframework.security.core.userdetails.User(user.getLoginName(), user.getPassword(), enables, accountNonExpired, credentialsNonExpired, accountNonLocked, grantedAuthorities);
        return userDetails;


    }


    /**
     * 根据用户获取该用户拥有的角色
     *
     * @param user
     * @return
     */
    private Set<GrantedAuthority> getGrantedAuthorities(SysUser user) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        List<SysRole> roles = sysRoleDao.selectByUserId(user.getUserId());
        if (roles != null) {
            for (SysRole role : roles) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleName()));
            }
        }
        return grantedAuthorities;
    }

}
