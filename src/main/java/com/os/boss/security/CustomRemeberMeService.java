package com.os.boss.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.RememberMeServices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/11
 */
public class CustomRemeberMeService implements RememberMeServices {

    Logger logger = LoggerFactory.getLogger(CustomRemeberMeService.class);

    public Authentication autoLogin(HttpServletRequest request, HttpServletResponse response) {

        logger.info("验证登录："+request);

        logger.info("验证登录，Response："+response);


        return null;
    }

    public void loginFail(HttpServletRequest request, HttpServletResponse response) {

    }

    public void loginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication successfulAuthentication) {

        logger.info("登录成功，Request："+request);

        logger.info("登录成功，Response："+response);

        logger.info("登录成功，successfulAuthentication："+successfulAuthentication);

        logger.info("登录成功，successfulAuthentication："+successfulAuthentication);

    }
}
