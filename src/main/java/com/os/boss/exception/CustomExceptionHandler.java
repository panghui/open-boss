package com.os.boss.exception;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义异常
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/11
 */
public class CustomExceptionHandler implements HandlerExceptionResolver {


    public ModelAndView resolveException(HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse,
                                         Object o, Exception e) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("ex", e);

//        return new ModelAndView("system/error-page",model);
        return new ModelAndView("system/error-page").addObject("model",model );
    }
}
