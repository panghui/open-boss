package com.os.boss.exception;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/2
 */
public class OBossException extends AuthenticationServiceException {

    public OBossException(String msg) {
        super(msg);
    }
}
