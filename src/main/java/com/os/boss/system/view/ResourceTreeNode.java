package com.os.boss.system.view;

/**
 * 角色权限展现层实体
 * @author luxd
 * @version 1.0
 * @since 2015/02/10
 */
public class ResourceTreeNode {
	/**
	 * 树ID
	 */
	private String id;
	
	/**
	 * 树根节点ID
	 */
    private String pId;
    
    /**
     * 节点名称对应角色名称
     */
    private String name;
    
    /**
     * 是否选中状态
     */
    private Boolean checked;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

}
