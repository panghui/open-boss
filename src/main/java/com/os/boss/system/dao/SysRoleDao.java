package com.os.boss.system.dao;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysRoleCondition;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.mapper.SysRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/1
 */
@Repository
public class SysRoleDao {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 添加角色
     *
     * @param record
     * @return
     */
    public int insertSelective(SysRole record){
        return sysRoleMapper.insertSelective(record);
    }

    /**
     * 根据主键删除角色
     *
     * @param roleId
     * @return
     */
    public int deleteByPrimaryKey(Long roleId){
        return sysRoleMapper.deleteByPrimaryKey(roleId);
    }

    /**
     * 更新角色信息
     *
     * @param record
     * @return
     */
    public int updateByPrimaryKeySelective(SysRole record){
        return sysRoleMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 通过用户ID获取用户所拥有的权限列表
     *
     * @param userId
     * @return
     */
    public List<SysRole> selectByUserId(Long userId){
        return sysRoleMapper.selectByUserId(userId);
    }


    /**
     * 获取所有角色列表
     *
     * @return
     */
    public List<SysRole> selectRoleList(){
        return sysRoleMapper.selectRoleList();
    }


    /**
     * 根据角色ID获取对应角色
     *
     * @param roleId
     * @return
     */
    public SysRole selectByPrimaryKey(Long roleId){
        return sysRoleMapper.selectByPrimaryKey(roleId);
    }

    /**
     * 根据条件，获取角色分页列表
     *
     * @param sysRoleCondition
     * @return
     */
    public Page<SysRole> selectRoleListPage(SysRoleCondition sysRoleCondition){
        List<SysRole> list = sysRoleMapper.selectRoleListPage(sysRoleCondition);
        sysRoleCondition.setList(list);
        return sysRoleCondition;
    }


    /**
     * 根据用户登录名称，获取该用户拥有的角色
     *
     * @param loginName
     * @return
     */
    public SysRole selectRoleNameByLoginName(String loginName){
        return sysRoleMapper.selectRoleNameByLoginName(loginName);
    }

}
