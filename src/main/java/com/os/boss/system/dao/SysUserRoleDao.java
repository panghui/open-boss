package com.os.boss.system.dao;

import com.os.boss.system.entitys.SysUserRole;
import com.os.boss.system.mapper.SysUserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/4
 */
@Repository
public class SysUserRoleDao {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    /**
     * 添加用户信息
     *
     * @param sysUserRole
     * @return
     */
    public int insert(SysUserRole sysUserRole){
        return sysUserRoleMapper.insert(sysUserRole);
    }


    /**
     * 根据用户ID，获取用户权限
     *
     * @param userId
     * @return
     */
    public SysUserRole selectByUserId(Long userId){
        return sysUserRoleMapper.selectByUserId(userId);
    }

    /**
     * 更新权限关系表
     *
     * @param record
     * @return
     */
    public int updateByPrimaryKeySelective(SysUserRole record){
        return sysUserRoleMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 根据用户ID，删除用户所拥有的角色
     *
     * @param userId
     * @return
     */
    public int deleteByUserId(Long userId){
        return sysUserRoleMapper.deleteByUserId(userId);
    }
}
