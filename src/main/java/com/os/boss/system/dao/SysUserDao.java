package com.os.boss.system.dao;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysUserCondition;
import com.os.boss.system.entitys.SysUser;
import com.os.boss.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/7/18
 */
@Repository
public class SysUserDao {

    @Autowired
    private SysUserMapper sysUserMapper;


    public int insert(SysUser sysUser){
        return sysUserMapper.insert(sysUser);
    }


    public SysUser selectByPrimaryKey(Long userId){
        return sysUserMapper.selectByPrimaryKey(userId);
    }

    /**
     * 根据登录名称，查询用户信息
     *
     * @param loginName
     * @return
     */
    public SysUser selectByLoginName(String loginName){
        return sysUserMapper.selectByLoginName(loginName);
    }

    /**
     * 根据条件，获取用户列表
     *
     * @param sysUserCondition
     * @return
     */
    public Page<SysUser> selectUserListPage(SysUserCondition sysUserCondition){

        List<SysUser> list = sysUserMapper.selectUserListPage(sysUserCondition);

        sysUserCondition.setList(list);

        return sysUserCondition;
    }

    /**
     * 根据用户ID修改用户信息
     *
     * @param sysUser
     * @return
     */
    public int updateByPrimaryKeySelective(SysUser sysUser){
        return sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    /**
     * 根据用户ID删除用户数据
     *
     * @param userId
     * @return
     */
    public int deleteByPrimaryKey(Long userId){
        return sysUserMapper.deleteByPrimaryKey(userId);
    }
}
