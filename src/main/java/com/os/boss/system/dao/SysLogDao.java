package com.os.boss.system.dao;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysLogCondition;
import com.os.boss.system.entitys.SysLog;
import com.os.boss.system.mapper.SysLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/9
 */
@Repository
public class SysLogDao {

    @Autowired
    private SysLogMapper sysLogMapper;

    /**
     * 新增日志
     *
     * @param record
     * @return
     */
    public int insertSelective(SysLog record){
        return sysLogMapper.insertSelective(record);
    }

    /**
     * 根据条件获取日志分页列表
     *
     * @param sysLogCondition
     * @return
     */
    public Page<SysLog> selectSysLogPageList(SysLogCondition sysLogCondition){
        List<SysLog> list = sysLogMapper.selectLogListPage(sysLogCondition);
        sysLogCondition.setList(list);
        return sysLogCondition;
    }

}
