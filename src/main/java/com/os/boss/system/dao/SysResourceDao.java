package com.os.boss.system.dao;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysResourceCondition;
import com.os.boss.system.entitys.SysResource;
import com.os.boss.system.mapper.SysResourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/5
 */
@Repository
public class SysResourceDao {

    @Autowired
    private SysResourceMapper sysResourceMapper;

    /**
     * 增加资源
     *
     * @param record
     * @return
     */
    public int insertSelective(SysResource record){
        return sysResourceMapper.insertSelective(record);
    }

    /**
     * 根据主键删除资源
     *
     * @param resId
     * @return
     */
    public int deleteByPrimaryKey(String resId){
        return sysResourceMapper.deleteByPrimaryKey(resId);
    }

    /**
     * 根据主键更新资源信息
     *
     * @param sysResource
     * @return
     */
    public int updateByPrimaryKeySelective(SysResource sysResource){
        return sysResourceMapper.updateByPrimaryKeySelective(sysResource);
    }

    /**
     * 根据主键获取资源
     *
     * @param resId
     * @return
     */
    public SysResource selectByPrimaryKey(String resId){
        return sysResourceMapper.selectByPrimaryKey(resId);
    }


    /**
     * 根据pid找到最大的子资源id
     *
     * @param sysPid
     * @return
     */
    public String findMaxChildResourceIdByPid(String sysPid){
        return sysResourceMapper.findMaxChildResourceIdByPid(sysPid);
    }

    /**
     * 分页获取资源列表
     *
     * @param sysResourceCondition
     * @return
     */
    public Page<SysResource> selectResourceListPage(SysResourceCondition sysResourceCondition){

        List<SysResource> list = sysResourceMapper.selectResourceListPage(sysResourceCondition);

        sysResourceCondition.setList(list);

        return sysResourceCondition;
    }


    /**
     * 查询所有父资源列表
     *
     * @return
     */
    public List<SysResource> selectResourceList(){
        return sysResourceMapper.selectResourceList();
    }

    /**
     * 根据Pid获取所有子节点资源列表
     *
     * @param resPid
     * @return
     */
    public List<SysResource> selectResourceListByResPid(String resPid){
        return sysResourceMapper.selectResourceListByResPid(resPid);
    }

    /**
     * 根据Pid 及用户所拥有的权限，获取该用户可操作节点
     *
     * @param map map中需要的KEY｛resPid,roleId｝
     * @return
     */
    public List<SysResource> selectUserResList(Map<String,Object> map){
        return sysResourceMapper.selectUserResList(map);
    }

}
