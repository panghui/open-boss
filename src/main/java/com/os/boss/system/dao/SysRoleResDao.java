package com.os.boss.system.dao;

import com.os.boss.system.entitys.SysRoleRes;
import com.os.boss.system.mapper.SysRoleResMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/8
 */
@Repository
public class SysRoleResDao {

    @Autowired
    private SysRoleResMapper sysRoleResMapper;

    /**
     * 添加角色与资源关系数据
     *
     * @param record
     * @return
     */
    public int insertSelective(SysRoleRes record){
        return sysRoleResMapper.insertSelective(record);
    }

    /**
     * 删除角色与资源表关系数据
     *
     * @param id
     * @return
     */
    public int deleteByPrimaryKey(Long id){
        return sysRoleResMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新角色与资源表关系数据
     *
     * @param record
     * @return
     */
    public int updateByPrimaryKeySelective(SysRoleRes record){
        return sysRoleResMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 查找角色与资源表关系数据
     *
     * @param id
     * @return
     */
    public SysRoleRes selectByPrimaryKey(Long id){
        return sysRoleResMapper.selectByPrimaryKey(id);
    }


    /**
     * 根据角色ID，获取该角色所拥有的资源列表
     *
     * @param roleId
     * @return
     */
    public List<SysRoleRes> selectResByRoleId(Long roleId){
        return sysRoleResMapper.selectResByRoleId(roleId);
    }

    /**
     * 根据角色ID，删除该角色所拥有的资源
     *
     * @param roleId
     * @return
     */
    public int deleteByRoleId(Long roleId){
        return sysRoleResMapper.deleteByRoleId(roleId);
    }

}
