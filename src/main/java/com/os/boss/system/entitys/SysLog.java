package com.os.boss.system.entitys;

import java.util.Date;

public class SysLog {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_log.log_id
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    private Long logId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_log.description
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_log.class_method
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    private String classMethod;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_log.ip
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    private String ip;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_log.operate_user
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    private String operateUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_log.create_time
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    private Date createTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_log.log_id
     *
     * @return the value of sys_log.log_id
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public Long getLogId() {
        return logId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_log.log_id
     *
     * @param logId the value for sys_log.log_id
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public void setLogId(Long logId) {
        this.logId = logId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_log.description
     *
     * @return the value of sys_log.description
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_log.description
     *
     * @param description the value for sys_log.description
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_log.class_method
     *
     * @return the value of sys_log.class_method
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public String getClassMethod() {
        return classMethod;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_log.class_method
     *
     * @param classMethod the value for sys_log.class_method
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod == null ? null : classMethod.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_log.ip
     *
     * @return the value of sys_log.ip
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public String getIp() {
        return ip;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_log.ip
     *
     * @param ip the value for sys_log.ip
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_log.operate_user
     *
     * @return the value of sys_log.operate_user
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public String getOperateUser() {
        return operateUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_log.operate_user
     *
     * @param operateUser the value for sys_log.operate_user
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public void setOperateUser(String operateUser) {
        this.operateUser = operateUser == null ? null : operateUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_log.create_time
     *
     * @return the value of sys_log.create_time
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_log.create_time
     *
     * @param createTime the value for sys_log.create_time
     *
     * @mbggenerated Tue Aug 09 10:31:17 CST 2016
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}