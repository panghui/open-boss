package com.os.boss.system.entitys;

import java.util.Date;
import java.util.List;

public class SysResource {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.res_id
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private String resId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.sys_pid
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private String resPid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.url
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private String url;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.level
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private Long level;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.isleaf
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private Integer isleaf;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.name
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private String name;

    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.status
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private Integer status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.creator
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.create_time
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.modifier
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_resource.modify_time
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    private Date modifyTime;

    // 构建子节点List
    public List<SysResource> childResourceList;

    public List<SysResource> getChildResourceList() {
        return childResourceList;
    }

    public void setChildResourceList(List<SysResource> childResourceList) {
        this.childResourceList = childResourceList;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.res_id
     *
     * @return the value of sys_resource.res_id
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public String getResId() {
        return resId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.res_id
     *
     * @param resId the value for sys_resource.res_id
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setResId(String resId) {
        this.resId = resId == null ? null : resId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.sys_pid
     *
     * @return the value of sys_resource.sys_pid
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public String getResPid() {
        return resPid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.sys_pid
     *
     * @param resPid the value for sys_resource.sys_pid
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setResPid(String resPid) {
        this.resPid = resPid == null ? null : resPid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.url
     *
     * @return the value of sys_resource.url
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public String getUrl() {
        return url;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.url
     *
     * @param url the value for sys_resource.url
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.level
     *
     * @return the value of sys_resource.level
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public Long getLevel() {
        return level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.level
     *
     * @param level the value for sys_resource.level
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setLevel(Long level) {
        this.level = level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.isleaf
     *
     * @return the value of sys_resource.isleaf
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public Integer getIsleaf() {
        return isleaf;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.isleaf
     *
     * @param isleaf the value for sys_resource.isleaf
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setIsleaf(Integer isleaf) {
        this.isleaf = isleaf;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.name
     *
     * @return the value of sys_resource.name
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.name
     *
     * @param name the value for sys_resource.name
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.status
     *
     * @return the value of sys_resource.status
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.status
     *
     * @param status the value for sys_resource.status
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.creator
     *
     * @return the value of sys_resource.creator
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.creator
     *
     * @param creator the value for sys_resource.creator
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.create_time
     *
     * @return the value of sys_resource.create_time
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.create_time
     *
     * @param createTime the value for sys_resource.create_time
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.modifier
     *
     * @return the value of sys_resource.modifier
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.modifier
     *
     * @param modifier the value for sys_resource.modifier
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_resource.modify_time
     *
     * @return the value of sys_resource.modify_time
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_resource.modify_time
     *
     * @param modifyTime the value for sys_resource.modify_time
     *
     * @mbggenerated Mon Jul 18 22:18:35 CST 2016
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}