package com.os.boss.system.service.impl;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysResourceCondition;
import com.os.boss.system.dao.SysResourceDao;
import com.os.boss.system.dao.SysUserDao;
import com.os.boss.system.dao.SysUserRoleDao;
import com.os.boss.system.entitys.SysResource;
import com.os.boss.system.entitys.SysUser;
import com.os.boss.system.entitys.SysUserRole;
import com.os.boss.system.service.SysResourceService;
import com.os.boss.utils.SessionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/5
 */
@Service("sysResourceService")
public class SysResourceServiceImpl implements SysResourceService {

    @Autowired
    private SysResourceDao sysResourceDao;

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    /**
     * 添加资源
     *
     * @param sysResource
     * @return
     */
    public int addSysResource(SysResource sysResource){
        bindResource(sysResource,1);
        return sysResourceDao.insertSelective(sysResource);
    }

    /**
     * 根据主键删除资源信息
     *
     * @param resId
     * @return
     */
    public int deleteSysResourceByKey(String resId){
        return sysResourceDao.deleteByPrimaryKey(resId);
    }

    /**
     * 更新资源信息
     *
     * @param sysResource
     * @return
     */
    public int updateSysResource(SysResource sysResource){
        bindResource(sysResource, 2);
        return sysResourceDao.updateByPrimaryKeySelective(sysResource);
    }

    /**
     * 更新资源状态
     *
     * @param sysResource
     * @return
     */
    public int updateSysResourceStatus(SysResource sysResource){
        bindResource(sysResource, 2);
        sysResource = sysResourceDao.selectByPrimaryKey(sysResource.getResId());

        if (1 == sysResource.getStatus()) {
            sysResource.setStatus(0);
        } else {
            sysResource.setStatus(1);
        }

        return sysResourceDao.updateByPrimaryKeySelective(sysResource);
    }

    /**
     * 根据主键获取资源对象
     *
     * @param resId
     * @return
     */
    public SysResource selectSysResourceByPrimaryKey(String resId){
        return sysResourceDao.selectByPrimaryKey(resId);
    }

    /**
     * 根据条件，分页获取资源列表
     *
     * @param sysResourceCondition
     * @return
     */
    public Page<SysResource> selectResourcePageList(SysResourceCondition sysResourceCondition){
        return sysResourceDao.selectResourceListPage(sysResourceCondition);
    }

    /**
     * 查询所有资源列表
     *
     * @return
     */
    public List<SysResource> selectResourceList(){

        // 根据登录用户名称，获取当前用户所拥有的权限
        String loginName = SessionUtil.getUserDetails().getUsername();
        SysUser sysUser = sysUserDao.selectByLoginName(loginName);
        SysUserRole sysUserRole = sysUserRoleDao.selectByUserId(sysUser.getUserId());

        // 组装Map条件
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("resPid","0");
        map.put("roleId",sysUserRole.getRoleId());

        // 获取所有父节点List
        List<SysResource> list = sysResourceDao.selectUserResList(map);

        if(list != null && list.size()>0){
            for(SysResource sysResource : list){

                // 组装获取子节点资源条件
                map.put("resPid",sysResource.getResId());

                // 查询所有父节点下的子节点List，并且放入到父节点中
                List<SysResource> listChild = sysResourceDao.selectUserResList(map);
                sysResource.setChildResourceList(listChild);
            }
        }

        return list;
    }

    /**
     * 组装sysResource实体
     * @param flag 1用于创建,其他值用于更新
     * @return
     */
    protected SysResource bindResource(SysResource sysResource, Integer flag) {

        String userName = SessionUtil.getUserDetails().getUsername();

        if (flag == 1) {
            sysResource.setStatus(1);
            // 获取父级权限资源的级别，子级+1
            if (-1 == sysResource.getLevel()) {
                sysResource.setLevel(1L);
            }else{
                sysResource.setLevel(sysResource.getLevel()+1);
            }
            sysResource.setResId(buildResourceId(sysResource.getResPid()));
            sysResource.setCreateTime(new Date());
            sysResource.setCreator(userName);
        }
        if(StringUtils.isNotBlank(sysResource.getUrl()) ){
            sysResource.setIsleaf(1);
        }else{
            sysResource.setIsleaf(0);
        }
        sysResource.setModifyTime(new Date());
        sysResource.setModifier(userName);
        return sysResource;
    }


    /**
     * 构建resourceId
     * @param pid 父级节点的id
     * @return
     */
    protected String buildResourceId(String pid) {
        // 获得最大的子资源ID
        String maxChildResourceId = sysResourceDao.findMaxChildResourceIdByPid(pid);
        String tempId = null;
        String resourceId = null;

        if(pid.equals("0")){
            Integer newId = Integer.parseInt(maxChildResourceId) + 1;
            if (newId < 10) {
                resourceId = "0" + newId;
            } else {
                resourceId = newId.toString();
            }
        }else{
            if(null != maxChildResourceId) {
                tempId = maxChildResourceId.toString().substring(pid.length());
                Integer newId = Integer.parseInt(tempId) + 1;
                if (newId < 10) {
                    resourceId = "0" + newId;
                } else {
                    resourceId = newId.toString();
                }
                resourceId = pid + resourceId;
            }else{
                resourceId = pid+"01";
            }
        }
        return resourceId;
    }

}
