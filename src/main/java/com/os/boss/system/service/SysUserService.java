package com.os.boss.system.service;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysUserCondition;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.entitys.SysUser;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/3
 */
public interface SysUserService {

    /**
     * 根据用户名，查询该用户是否已存在
     *
     * @param loginName
     * @return 1=已存在,0-不存在
     */
    public int selectSysUserByLoginName(String loginName);

    /**
     * 根据条件获取用户列表信息以及分页
     *
     * @param sysUserCondition
     * @return
     */
    public Page<SysUser> selectSysUserListPage(SysUserCondition sysUserCondition);

    /**
     * 根据用户ID，修改用户状态
     *
     * @param sysUser
     * @return
     */
    public int updateSysUserStatus(SysUser sysUser);

    /**
     * 根据用户ID删除用户数据
     *
     * @param userId
     * @return
     */
    public int deleteUserByPrimaryKey(Long userId);

    /**
     * 添加用户，并且维护用户角色
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    public int addUser(SysUser sysUser,Long roleId);

    /**
     * 根据用户ID，获取用户权限ID
     *
     * @param userId
     * @return
     */
    public long selectSysUserRoleByUserId(Long userId);

    /**
     * 根据用户ID，获取用户信息
     *
     * @param userId
     * @return
     */
    public SysUser selectSysUserByUserId(Long userId);

    /**
     * 根据用户名，输入密码，判断输入旧密码是否正确
     *
     * @param password
     * @param loginName
     * @return
     */
    public int checkOldPassword(String password,String loginName);


    /**
     * 修改用户，并且维护用户角色
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    public int updateUser(SysUser sysUser,Long roleId);


    /**
     * 根据当前登录用户，获取该用户拥有的角色
     *
     * @return
     */
    public SysRole selectCurrentUserRole();

}
