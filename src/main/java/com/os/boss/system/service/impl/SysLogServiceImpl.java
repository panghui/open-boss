package com.os.boss.system.service.impl;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysLogCondition;
import com.os.boss.system.dao.SysLogDao;
import com.os.boss.system.entitys.SysLog;
import com.os.boss.system.service.SysLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/9
 */
@Service("sysLogService")
public class SysLogServiceImpl implements SysLogService{

    @Autowired
    private SysLogDao sysLogDao;

    /**
     * 根据条件获取日志分页列表
     *
     * @param sysLogCondition
     * @return
     */
    public Page<SysLog> selectSysLogPageList(SysLogCondition sysLogCondition){
        if(StringUtils.isNotBlank(sysLogCondition.getStartDateTime())){
            String startDateTime = sysLogCondition.getStartDateTime()+" 00:00:00";
            sysLogCondition.setStartDateTime(startDateTime);
        }

        if(StringUtils.isNotBlank(sysLogCondition.getEndDateTime())){
            String endDateTime = sysLogCondition.getEndDateTime()+" 23:59:59";
            sysLogCondition.setEndDateTime(endDateTime);
        }
        return sysLogDao.selectSysLogPageList(sysLogCondition);
    }

}
