package com.os.boss.system.service;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysLogCondition;
import com.os.boss.system.entitys.SysLog;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/9
 */
public interface SysLogService {

    /**
     * 根据条件获取日志分页列表
     *
     * @param sysLogCondition
     * @return
     */
    public Page<SysLog> selectSysLogPageList(SysLogCondition sysLogCondition);

}
