package com.os.boss.system.service.impl;

import com.os.boss.exception.OBossException;
import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysRoleCondition;
import com.os.boss.system.dao.SysResourceDao;
import com.os.boss.system.dao.SysRoleDao;
import com.os.boss.system.dao.SysRoleResDao;
import com.os.boss.system.entitys.SysResource;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.entitys.SysRoleRes;
import com.os.boss.system.service.SysRoleService;
import com.os.boss.system.view.ResourceTreeNode;
import com.os.boss.utils.SessionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/4
 */
@Service("sysRoleService")
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private SysResourceDao sysResourceDao;

    @Autowired
    private SysRoleResDao sysRoleResDao;

    /**
     * 根据角色ID，获取角色信息
     *
     * @param roleId
     * @return
     */
    public SysRole selectSysRoleByKey(Long roleId){
        return sysRoleDao.selectByPrimaryKey(roleId);
    }

    /**
     * 添加角色
     *
     * @param sysRole
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Exception.class})
    public int addSysRole(SysRole sysRole, String resIds) {

        String userName = SessionUtil.getUserDetails().getUsername();

        sysRole.setStatus(1);
        sysRole.setCreateTime(new Date());
        sysRole.setCreator(userName);

        int isSuccess = sysRoleDao.insertSelective(sysRole);

        if (isSuccess > 0) {

            if(StringUtils.isBlank(resIds)){
                throw new RuntimeException("资源列表为空");
            }

            String[] resIdArray = resIds.split(",");

            for (int i = 0; i < resIdArray.length; i++) {

                SysRoleRes sysRoleRes = new SysRoleRes();
                sysRoleRes.setRoleId(sysRole.getRoleId());
                sysRoleRes.setResId(resIdArray[i]);
                sysRoleRes.setCreateTime(new Date());
                sysRoleRes.setCreator(userName);

                isSuccess = sysRoleResDao.insertSelective(sysRoleRes);

                if (isSuccess <= 0) {
                    throw new RuntimeException("添加角色关系失败");
                }

            }

            return isSuccess;

        } else {
            throw new RuntimeException("添加角色失败");
        }

    }

    /**
     * 删除角色
     *
     * @param roleId
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Exception.class})
    public int delSysRoleByKey(Long roleId) {

        int isSuccess = sysRoleDao.deleteByPrimaryKey(roleId);

        if(isSuccess > 0){

            isSuccess = sysRoleResDao.deleteByRoleId(roleId);

            if(isSuccess >0){
                return  isSuccess;
            }else{
                throw new RuntimeException("删除角色资源失败");
            }

        }else{
            throw new RuntimeException("删除角色失败");
        }

    }

    /**
     * 更新角色
     *
     * @param sysRole
     * @param resIds
     * @param isOperate
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Exception.class})
    public int updateSysRole(SysRole sysRole,String resIds,String isOperate){

        String userName = SessionUtil.getUserDetails().getUsername();

        sysRole.setModifyTime(new Date());
        sysRole.setModifier(userName);

        int isSuccess = sysRoleDao.updateByPrimaryKeySelective(sysRole);

        if (isSuccess > 0) {

            if(isSuccess >0){

                // 如果做了更新资源操作，进行操作数据库，如果没有操作，不做修改
                if("1".equals(isOperate)){

                    isSuccess = sysRoleResDao.deleteByRoleId(sysRole.getRoleId());

                    String[] resIdArray = resIds.split(",");

                    for (int i = 0; i < resIdArray.length; i++) {

                        SysRoleRes sysRoleRes = new SysRoleRes();
                        sysRoleRes.setRoleId(sysRole.getRoleId());
                        sysRoleRes.setResId(resIdArray[i]);
                        sysRoleRes.setModifyTime(new Date());
                        sysRoleRes.setModifier(userName);

                        isSuccess = sysRoleResDao.insertSelective(sysRoleRes);

                        if (isSuccess <= 0) {
                            throw new RuntimeException("添加角色关系失败");
                        }

                    }
                }

                return isSuccess;
            }else{
                throw new RuntimeException("角色原有资源删除失败");
            }

        } else {
            throw new RuntimeException("添加角色失败");
        }

    }

    /**
     * 获取所有角色列表
     *
     * @return
     */
    public List<SysRole> selectRoleList() {
        return sysRoleDao.selectRoleList();
    }

    /**
     * 根据条件，获取角色分页列表信息
     *
     * @param sysRoleCondition
     * @return
     */
    public Page<SysRole> selectRolePageList(SysRoleCondition sysRoleCondition) {
        return sysRoleDao.selectRoleListPage(sysRoleCondition);
    }

    /**
     * 获取所有资源节点，组装成Tree节点
     *
     * @return
     */
    public List<ResourceTreeNode> selecctResourceList() {

        List<ResourceTreeNode> listTreeNode = new ArrayList<ResourceTreeNode>();

        List<SysResource> listResource = sysResourceDao.selectResourceList();

        if (listResource != null && listResource.size() > 0) {
            for (SysResource sysResource : listResource) {
                ResourceTreeNode resourceTreeNode = new ResourceTreeNode();
                resourceTreeNode.setId(sysResource.getResId());
                resourceTreeNode.setpId(sysResource.getResPid());
                resourceTreeNode.setName(sysResource.getName());
                listTreeNode.add(resourceTreeNode);
            }
        }

        return listTreeNode;

    }


    /**
     * 获取该角色所拥有的资源节点
     *
     * @param roleId
     * @return
     */
    public List<ResourceTreeNode> selectResourceListByRoleId(Long roleId) {

        List<ResourceTreeNode> list = selecctResourceList();

        List<SysRoleRes> listRoleRes = sysRoleResDao.selectResByRoleId(roleId);

        if (list != null && list.size() > 0 && listRoleRes != null && listRoleRes.size() > 0) {

            for (SysRoleRes sysRoleRes : listRoleRes) {

                for (ResourceTreeNode resourceTreeNode : list) {

                    if (resourceTreeNode.getId().equals(sysRoleRes.getResId())) {
                        resourceTreeNode.setChecked(true);
                        break;
                    }
                }
            }
        }

        return list;

    }

}
