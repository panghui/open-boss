package com.os.boss.system.service.impl;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysUserCondition;
import com.os.boss.system.dao.SysRoleDao;
import com.os.boss.system.dao.SysUserDao;
import com.os.boss.system.dao.SysUserRoleDao;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.entitys.SysUser;
import com.os.boss.system.entitys.SysUserRole;
import com.os.boss.system.service.SysUserService;
import com.os.boss.utils.IdWorker;
import com.os.boss.utils.SessionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/3
 */
@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private Md5PasswordEncoder md5PasswordEncoder;

    /**
     * 根据用户名，查询该用户是否已存在
     *
     * @param loginName
     * @return 1=已存在,0-不存在
     */
    public int selectSysUserByLoginName(String loginName) {

        SysUser sysUser = sysUserDao.selectByLoginName(loginName);
        if (sysUser != null) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 根据条件获取用户列表信息以及分页
     *
     * @param sysUserCondition
     * @return
     */
    public Page<SysUser> selectSysUserListPage(SysUserCondition sysUserCondition) {
        return sysUserDao.selectUserListPage(sysUserCondition);
    }


    /**
     * 根据用户ID，修改用户状态
     *
     * @param sysUser
     * @return
     */
    @Transactional
    public int updateSysUserStatus(SysUser sysUser) {

        sysUser = sysUserDao.selectByPrimaryKey(sysUser.getUserId());

        if (1 == sysUser.getStatus()) {
            sysUser.setStatus(0);
        } else {
            sysUser.setStatus(1);
        }

        return sysUserDao.updateByPrimaryKeySelective(sysUser);
    }


    /**
     * 根据用户ID删除用户数据
     *
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Exception.class})
    public int deleteUserByPrimaryKey(Long userId) {

        int isSuccess = sysUserDao.deleteByPrimaryKey(userId);

        if(isSuccess > 0){

            isSuccess = sysUserRoleDao.deleteByUserId(userId);

            if(isSuccess > 0){
                return isSuccess;
            }else{
                throw new RuntimeException("删除用户角色失败");
            }

        }else{
            throw new RuntimeException("删除用户失败");
        }

    }


    /**
     * 添加用户，并且维护用户角色
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    @Transactional
    public int addUser(SysUser sysUser, Long roleId) {

        Long userId = idWorker.nextId();
        sysUser.setUserId(userId);
        // 使用Spring Security 对密码加密，通过登录名称动态加盐值
        sysUser.setPassword(md5PasswordEncoder.encodePassword(sysUser.getPassword(), sysUser.getLoginName()));
        sysUser.setStatus(1);
        sysUser.setCreateTime(new Date());
        if (sysUserDao.insert(sysUser) > 0) {

            String userName = SessionUtil.getUserDetails().getUsername();

            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(userId);
            sysUserRole.setRoleId(roleId);
            sysUserRole.setCreator(userName);
            sysUserRole.setCreateTime(new Date());

            if (sysUserRoleDao.insert(sysUserRole) > 0) {
                return 1;
            } else {
                return 0;
            }

        } else {
            return 0;
        }

    }


    /**
     * 根据用户ID，获取用户权限ID
     *
     * @param userId
     * @return
     */
    public long selectSysUserRoleByUserId(Long userId) {
        SysUserRole sysUserRole = sysUserRoleDao.selectByUserId(userId);
        if (sysUserRole != null) {
            return sysUserRole.getRoleId();
        }
        return 0;
    }


    /**
     * 根据用户ID，获取用户信息
     *
     * @param userId
     * @return
     */
    public SysUser selectSysUserByUserId(Long userId) {
        return sysUserDao.selectByPrimaryKey(userId);
    }

    /**
     * 根据用户名，输入密码，判断输入旧密码是否正确
     *
     * @param password
     * @param loginName
     * @return
     */
    public int checkOldPassword(String password, String loginName) {
        SysUser sysUser = sysUserDao.selectByLoginName(loginName);
        boolean isCheck = md5PasswordEncoder.isPasswordValid(sysUser.getPassword(), password, loginName);
        if(isCheck){
            return 1;
        }else{
            return 0;
        }
    }


    /**
     * 修改用户，并且维护用户角色
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    public int updateUser(SysUser sysUser,Long roleId){

        if(StringUtils.isNotBlank(sysUser.getPassword())){
            // 使用Spring Security 对密码加密，通过登录名称动态加盐值
            sysUser.setPassword(md5PasswordEncoder.encodePassword(sysUser.getPassword(), sysUser.getLoginName()));
        }

        sysUser.setModifyTime(new Date());
        if (sysUserDao.updateByPrimaryKeySelective(sysUser) > 0) {

            SysUserRole sysUserRole = sysUserRoleDao.selectByUserId(sysUser.getUserId());
            sysUserRole.setRoleId(roleId);
            sysUserRole.setModifyTime(new Date());
            sysUserRole.setModifier(SessionUtil.getUserDetails().getUsername());

            if (sysUserRoleDao.updateByPrimaryKeySelective(sysUserRole) > 0) {
                return 1;
            } else {
                return 0;
            }

        } else {
            return 0;
        }
    }

    /**
     * 根据当前登录用户，获取该用户拥有的角色
     *
     * @return
     */
    public SysRole selectCurrentUserRole(){

        String loginName = SessionUtil.getUserDetails().getUsername();

        return sysRoleDao.selectRoleNameByLoginName(loginName);
    }
}
