package com.os.boss.system.service;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysResourceCondition;
import com.os.boss.system.entitys.SysResource;

import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/5
 */
public interface SysResourceService {

    /**
     * 添加资源
     *
     * @param sysResource
     * @return
     */
    public int addSysResource(SysResource sysResource);

    /**
     * 根据主键删除资源信息
     *
     * @param resId
     * @return
     */
    public int deleteSysResourceByKey(String resId);

    /**
     * 更新资源信息
     *
     * @param sysResource
     * @return
     */
    public int updateSysResource(SysResource sysResource);

    /**
     * 更新资源状态
     *
     * @param sysResource
     * @return
     */
    public int updateSysResourceStatus(SysResource sysResource);

    /**
     * 根据主键获取资源对象
     *
     * @param resId
     * @return
     */
    public SysResource selectSysResourceByPrimaryKey(String resId);

    /**
     * 根据条件，分页获取资源列表
     *
     * @param sysResourceCondition
     * @return
     */
    public Page<SysResource> selectResourcePageList(SysResourceCondition sysResourceCondition);

    /**
     * 查询所有资源列表
     *
     * @return
     */
    public List<SysResource> selectResourceList();

}
