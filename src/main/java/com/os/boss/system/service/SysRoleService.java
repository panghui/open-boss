package com.os.boss.system.service;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysRoleCondition;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.view.ResourceTreeNode;

import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/4
 */
public interface SysRoleService {

    /**
     * 根据角色ID，获取角色信息
     *
     * @param roleId
     * @return
     */
    public SysRole selectSysRoleByKey(Long roleId);

    /**
     * 添加角色
     *
     * @param sysRole
     * @return
     */
    public int addSysRole(SysRole sysRole,String resIds);

    /**
     * 删除角色
     *
     * @param roleId
     * @return
     */
    public int delSysRoleByKey(Long roleId);

    /**
     * 更新角色
     *
     * @param sysRole
     * @param resIds
     * @param isOperate
     * @return
     */
    public int updateSysRole(SysRole sysRole,String resIds,String isOperate);

    /**
     * 获取所有角色列表
     *
     * @return
     */
    public List<SysRole> selectRoleList();

    /**
     * 根据条件，获取角色分页列表信息
     *
     * @param sysRoleCondition
     * @return
     */
    public Page<SysRole> selectRolePageList(SysRoleCondition sysRoleCondition);

    /**
     * 获取所有资源节点，组装成Tree节点
     *
     * @return
     */
    public List<ResourceTreeNode> selecctResourceList();

    /**
     * 获取该角色所拥有的资源节点
     *
     * @param roleId
     * @return
     */
    public List<ResourceTreeNode> selectResourceListByRoleId(Long roleId);
}
