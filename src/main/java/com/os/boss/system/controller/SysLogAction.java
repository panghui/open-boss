package com.os.boss.system.controller;

import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysLogCondition;
import com.os.boss.system.entitys.SysLog;
import com.os.boss.system.service.SysLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 日志控制类
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/9
 */
@Controller
@RequestMapping(value = "/log")
public class SysLogAction {

    @Autowired
    private SysLogService sysLogService;

    /**
     * 根据条件获取日志列表信息
     *
     * @param model
     * @param sysLogCondition
     * @return
     */
    @RequestMapping("/showLogList")
    public String showLogList(Model model, SysLogCondition sysLogCondition){

        Page<SysLog> list = sysLogService.selectSysLogPageList(sysLogCondition);

        if(StringUtils.isNotBlank(sysLogCondition.getStartDateTime())){
            String startDateTime = sysLogCondition.getStartDateTime().substring(0,sysLogCondition.getStartDateTime().indexOf(" "));
            sysLogCondition.setStartDateTime(startDateTime);
        }
        if(StringUtils.isNotBlank(sysLogCondition.getEndDateTime())){
            String endDateTime = sysLogCondition.getEndDateTime().substring(0,sysLogCondition.getEndDateTime().indexOf(" "));
            sysLogCondition.setEndDateTime(endDateTime);
        }

        if(list != null){
            model.addAttribute("count",list.getTotalRecord());
        }

        model.addAttribute("condition", sysLogCondition);
        model.addAttribute("pageObject", list);

        return "/system/system-log-list";
    }

}
