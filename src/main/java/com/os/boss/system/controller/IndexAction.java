package com.os.boss.system.controller;

import com.os.boss.system.entitys.SysResource;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.service.SysResourceService;
import com.os.boss.system.service.SysUserService;
import com.os.boss.utils.Captcha;
import com.os.boss.utils.SessionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/7/30
 */
@Controller
public class IndexAction {

    @Autowired
    private SysResourceService sysResourceService;

    @Autowired
    private SysUserService sysUserService;

    /**
     * 跳转到登录页面
     *
     * @return
     */
    @RequestMapping(value="/showLogin")
    public String showLogin(String tag,Model model){
        model.addAttribute("tag",tag);
//        Object obj =  SecurityContextHolder.getContext().getAuthentication();
//        if(null == obj){
//            return "base/login";
//        }
//        String userName = SessionUtil.getUserDetails().getUsername();
//        if(StringUtils.isNotBlank(userName)){
//            return "base/index";
//        }else{
//            return "base/login";
//        }
        return "base/login";
    }

    /**
     * 登录成功以后，进入首页
     *
     * @return
     */
    @RequestMapping(value = "/showIndex")
    public String showIndex(Model model){
        List<SysResource> listRes = sysResourceService.selectResourceList();
        SysRole currentUserRole = sysUserService.selectCurrentUserRole();
        String loginName = SessionUtil.getUserDetails().getUsername();
        model.addAttribute("currentUserRole",currentUserRole);
        model.addAttribute("loginName",loginName);
        model.addAttribute("listRes",listRes);
        return "base/index";
    }

    /**
     * 获取验证码
     * @param resp
     * @param session
     * @throws java.io.IOException
     */
    @RequestMapping("/drawCheckCode")
    public void drawCheckCode(HttpServletResponse resp,HttpSession session) throws IOException {
        resp.setContentType("image/jpg");
        int width = 100;
        int height = 37;
        Captcha c = Captcha.getInstance();
        c.set(width, height);
        String checkcode = c.generateCheckcode();
        session.setAttribute("checkcode", checkcode);
        OutputStream os = resp.getOutputStream();
        ImageIO.write(c.generateCheckImg(checkcode), "jpg", os);
    }

}
