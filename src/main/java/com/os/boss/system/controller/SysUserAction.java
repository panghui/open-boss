package com.os.boss.system.controller;

import com.os.boss.common.aop.MethodLog;
import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysUserCondition;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.entitys.SysUser;
import com.os.boss.system.service.SysRoleService;
import com.os.boss.system.service.SysUserService;
import com.os.boss.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 系统用户控制层
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/3
 */
@Controller
@RequestMapping(value = "/sysUser")
public class SysUserAction {

    Logger logger = LoggerFactory.getLogger(SysUserAction.class);

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 添加用户
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    @RequestMapping("/addUser")
    @ResponseBody
    @MethodLog(remark = "添加用户")
    public String addUser(SysUser sysUser,Long roleId){
        return CommonUtil.toString(sysUserService.addUser(sysUser, roleId));
    }

    /**
     * 获取用户列表信息
     *
     * @param sysUserCondition
     * @param model
     * @return
     */
    @RequestMapping("/getUserList")
    public String getUserList(SysUserCondition sysUserCondition,Model model){

        Page<SysUser> list = sysUserService.selectSysUserListPage(sysUserCondition);

        if(list != null){
            model.addAttribute("count",list.getTotalRecord());
        }

        model.addAttribute("condition", sysUserCondition);
        model.addAttribute("pageObject", list);

        return "system/admin-list";

    }

    /**
     * 更改用户状态，返回成功与否信息
     *
     * @param sysUser
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateSysUserStatus")
    @MethodLog(remark = "更改用户状态，返回成功与否信息")
    public String updateSysUserStatus(SysUser sysUser){
        return CommonUtil.toString(sysUserService.updateSysUserStatus(sysUser));
    }


    /**
     * 根据用户ID，删除用户数据
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/deleteUser")
    @MethodLog(remark = "根据用户ID，删除用户数据")
    public String deleteUser(Long userId){
        return CommonUtil.toString(sysUserService.deleteUserByPrimaryKey(userId));
    }

    /**
     * 跳转到用户添加页面
     *
     * @return
     */
    @RequestMapping("/showAdminAdd")
    public String showAdminAdd(Model model){

        List<SysRole> listRole = sysRoleService.selectRoleList();

        model.addAttribute("listRole",listRole);

        return "system/admin-add";
    }

    /**
     * 检查用户是否已存在
     *
     * @param loginName
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkUserExist")
    public String checkUserExist(String loginName){
        return CommonUtil.toString(sysUserService.selectSysUserByLoginName(loginName));
    }

    /**
     * 跳转到用户添加页面
     *
     * @return
     */
    @RequestMapping("/showAdminEdit")
    public String showAdminEdit(Model model,Long userId){

        List<SysRole> listRole = sysRoleService.selectRoleList();
        SysUser sysUser = sysUserService.selectSysUserByUserId(userId);
        Long roleId = sysUserService.selectSysUserRoleByUserId(userId);

        model.addAttribute("listRole",listRole);
        model.addAttribute("sysUser",sysUser);
        model.addAttribute("roleId",roleId);

        return "system/admin-edit";
    }

    /**
     * 根据用户名，判断输入的旧密码是否正确
     *
     * @param passwrod
     * @param loginName
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkPassword")
    public String checkPassword(String passwrod,String loginName){
        return CommonUtil.toString(sysUserService.checkOldPassword(passwrod,loginName));
    }

    /**
     * 修改用户信息
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateSysUser")
    @MethodLog(remark = "修改用户信息")
    public String updateSysUser(SysUser sysUser,Long roleId){
        return CommonUtil.toString(sysUserService.updateUser(sysUser,roleId));
    }

}
