package com.os.boss.system.controller;

import com.os.boss.common.aop.MethodLog;
import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysResourceCondition;
import com.os.boss.system.entitys.SysResource;
import com.os.boss.system.service.SysResourceService;
import com.os.boss.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 操作权限Action
 *
 * @author panghui
 * @version 1.0
 * @since 2016/7/26
 */
@Controller
@RequestMapping(value = "/permission")
public class PermissionAction {

    @Autowired
    private SysResourceService sysResourceService;

    /**
     * 获取列表
     *
     * @param model
     * @return
     */
    @RequestMapping("/showPermission")
    public String showPermission(Model model,SysResourceCondition sysResourceCondition){

        Page<SysResource> list = sysResourceService.selectResourcePageList(sysResourceCondition);
        if(list != null){
            model.addAttribute("count",list.getTotalRecord());
        }

        model.addAttribute("condition", sysResourceCondition);
        model.addAttribute("pageObject", list);

        return "system/admin-permission";

    }

    /**
     * 跳转新增父节点页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/showAddFatherNode/{resId}")
    public String showAddFatherNode(Model model,@PathVariable String resId){

        SysResource sysResource = sysResourceService.selectSysResourceByPrimaryKey(resId);

        if(sysResource != null){
            model.addAttribute("sysResource",sysResource);
            model.addAttribute("isIcon",0);
        }else{
            sysResource = new SysResource();
            sysResource.setResId("0");
            sysResource.setLevel(-1L);
            sysResource.setName("无父节点");
            model.addAttribute("sysResource",sysResource);
            model.addAttribute("isIcon",1);
        }

        return "system/admin-permission-add";

    }

    /**
     * 添加资源
     *
     * @param sysResource
     * @return
     */
    @ResponseBody
    @RequestMapping("/addResourceNode")
    @MethodLog(remark = "添加资源")
    public String addResourceNode(SysResource sysResource){
        return CommonUtil.toString(sysResourceService.addSysResource(sysResource));
    }

    /**
     * 根据主键删除资源信息
     *
     * @param resId
     * @return
     */
    @ResponseBody
    @RequestMapping("/deleteResource")
    @MethodLog(remark = "根据主键删除资源信息")
    public String deleteResource(String resId){
        return CommonUtil.toString(sysResourceService.deleteSysResourceByKey(resId));
    }

    /**
     * 跳转到更新资源页面
     *
     * @param resId
     * @return
     */
    @RequestMapping("/showUpdateResource")
    public String showUpdateResource(Model model, String resId){
        SysResource sysResource = sysResourceService.selectSysResourceByPrimaryKey(resId);
        model.addAttribute("sysResource",sysResource);

        SysResource fatherSysResource = sysResourceService.selectSysResourceByPrimaryKey(sysResource.getResPid());
        if(fatherSysResource == null){
            fatherSysResource = new SysResource();
            fatherSysResource.setName("无父节点");
            model.addAttribute("isIcon",1);
        }else{
            model.addAttribute("isIcon",0);
        }
        model.addAttribute("fatherSysResource",fatherSysResource);
        return "system/admin-permission-edit";
    }

    /**
     * 更新资源信息
     *
     * @param sysResource
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateResource")
    @MethodLog(remark = "更新资源信息")
    public String updateResource(SysResource sysResource){
        return CommonUtil.toString(sysResourceService.updateSysResource(sysResource));
    }

    /**
     * 更新资源可用状态
     *
     * @param sysResource
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateSysResourceStatus")
    @MethodLog(remark = "更新资源可用状态")
    public String updateSysResourceStatus(SysResource sysResource){
        return CommonUtil.toString(sysResourceService.updateSysResourceStatus(sysResource));
    }

}
