package com.os.boss.system.controller;

import com.os.boss.common.aop.MethodLog;
import com.os.boss.interceptor.Page;
import com.os.boss.system.condition.SysRoleCondition;
import com.os.boss.system.entitys.SysRole;
import com.os.boss.system.service.SysRoleService;
import com.os.boss.system.view.ResourceTreeNode;
import com.os.boss.utils.CommonUtil;
import com.os.boss.utils.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 操作角色Action
 *
 * @author panghui
 * @version 1.0
 * @since 2016/7/29
 */
@Controller
@RequestMapping(value = "/role")
public class SysRoleAction {

    Logger logger = LoggerFactory.getLogger(SysRoleAction.class);

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 获取角色列表信息
     *
     * @param model
     * @param sysRoleCondition
     * @return
     */
    @RequestMapping(value = "/showRole")
    public String showPermission(Model model,SysRoleCondition sysRoleCondition){

        Page<SysRole> list = sysRoleService.selectRolePageList(sysRoleCondition);

        if(list != null){
            model.addAttribute("count",list.getTotalRecord());
        }

        model.addAttribute("condition", sysRoleCondition);
        model.addAttribute("pageObject", list);

        return "system/admin-role";
    }

    /**
     * 跳转到添加角色页面
     *
     * @return
     */
    @RequestMapping("/showAddRole")
    public String showAddPermission(Model model){

        List<ResourceTreeNode> listTreeNode = sysRoleService.selecctResourceList();

        String jsonResult = JsonUtil.ObjectToJson(listTreeNode);

        logger.info("JSON数据格式："+jsonResult);

        model.addAttribute("treeData",jsonResult);

        return "system/admin-role-add";
    }

    /**
     * 添加角色
     *
     * @param sysRole
     * @param resIds
     * @return
     */
    @ResponseBody
    @RequestMapping("/addRole")
    @MethodLog(remark = "添加角色")
    public String addRole(SysRole sysRole,String resIds){
        return CommonUtil.toString(sysRoleService.addSysRole(sysRole,resIds));
    }


    /**
     * 跳转到更新角色页面
     *
     * @param model
     * @param sysRole
     * @return
     */
    @RequestMapping("/showUpdateRole")
    public String showUpdateRole(Model model,SysRole sysRole){

        sysRole = sysRoleService.selectSysRoleByKey(sysRole.getRoleId());

        List<ResourceTreeNode> listTreeNode = sysRoleService.selectResourceListByRoleId(sysRole.getRoleId());

        String jsonResult = JsonUtil.ObjectToJson(listTreeNode);

        logger.info("资源节点数据："+jsonResult);

        model.addAttribute("treeData",jsonResult);
        model.addAttribute("sysRole",sysRole);
        model.addAttribute("isOperate","0");

        return "system/admin-role-update";

    }

    /**
     * 更新角色
     *
     * @param sysRole
     * @param resIds
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateRole")
    @MethodLog(remark = "更新角色")
    public String updateRole(SysRole sysRole,String resIds,String isOperate){
        return CommonUtil.toString(sysRoleService.updateSysRole(sysRole,resIds,isOperate));
    }

    /**
     * 删除角色
     *
     * @param roleId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delRole")
    @MethodLog(remark = "删除角色")
    public String delRole(Long roleId){
        return CommonUtil.toString(sysRoleService.delSysRoleByKey(roleId));
    }

}
