package com.os.boss.system.condition;

import com.os.boss.interceptor.Page;
import com.os.boss.system.entitys.SysLog;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/9
 */
public class SysLogCondition extends Page<SysLog>{

    private String startDateTime;

    private String endDateTime;

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }
}
