package com.os.boss.system.condition;

import com.os.boss.interceptor.Page;
import com.os.boss.system.entitys.SysResource;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/5
 */
public class SysResourceCondition extends Page<SysResource> {

    private String resName;

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }
}
