package com.os.boss.system.condition;

import com.os.boss.interceptor.Page;
import com.os.boss.system.entitys.SysRole;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/7
 */
public class SysRoleCondition extends Page<SysRole> {

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
