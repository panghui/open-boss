package com.os.boss.system.condition;

import com.os.boss.interceptor.Page;
import com.os.boss.system.entitys.SysUser;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/7/28
 */
public class SysUserCondition extends Page<SysUser> {

    private String loginName;

    private String name;

    private String tel;

    private String email;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
