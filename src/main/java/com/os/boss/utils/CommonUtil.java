package com.os.boss.utils;

/**
 * @author panghui
 * @version 1.0
 * @since 2016/8/3
 */
public class CommonUtil {

    public static String toString(Object o) {
        return o != null?o.toString():null;
    }
}
