package com.os.boss.utils;

import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.util.concurrent.RejectedExecutionException;

/**
 * 线程池工具类
 *
 * @author panghui
 * @version 1.0
 * @since 2016/10/21
 */
public class ThreadPoolUtil {

    private static Logger logger = Logger.getLogger(ThreadPoolUtil.class);

    private TaskExecutor taskExecutor;

    private ThreadPoolUtil() {
        logger.debug("--------------------线程池建立------------");
    }

    public ThreadPoolUtil(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void execute(Runnable task) {
        try {
            taskExecutor.execute(task);
        } catch (RejectedExecutionException e){
            e.printStackTrace();
            logger.debug("----------------------超出支持的最大的线程数-----------------");
        }
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

}
