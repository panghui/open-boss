package com.os.boss.utils;

import com.alibaba.fastjson.JSON;

public class JsonUtil {

	/**
	 * Object 转换JSON
	 *
	 * @param obj
	 * @return
	 */
	public static String ObjectToJson(Object obj){
		return JSON.toJSONString(obj);
	}




}
