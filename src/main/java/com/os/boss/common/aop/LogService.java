package com.os.boss.common.aop;

import com.os.boss.system.dao.SysLogDao;
import com.os.boss.system.entitys.SysLog;
import com.os.boss.utils.SessionUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 记录操作日志
 * 把这个类作为一个切面
 * 1. 放入Ioc容器，使用@Component
 * 2. 申请为切面@Aspect
 *
 * @author panghui
 * @version 1.0
 * @since 2016/8/8
 */
@Component
@Aspect
public class LogService {

    Logger logger = LoggerFactory.getLogger(LogService.class);

    @Autowired
    private SysLogDao sysLogDao;

    @Pointcut("@annotation(com.os.boss.common.aop.MethodLog)")
    public void methodCachePointcut() {
    }

    //    @Before("methodCachePointcut()")
    @After("methodCachePointcut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        //请求的IP
        String ip = request.getRemoteAddr();
        logger.info("IP地址："+ip);
        // 请求类及类方法
        String classMethod = joinPoint.getTarget().getClass().getName()
                + "." + joinPoint.getSignature().getName() + "()";
        try {
            logger.info("切面日志调用方法开始......");
            SysLog sysLog = new SysLog();
            sysLog.setDescription(getMthodRemark(joinPoint));
            sysLog.setClassMethod(classMethod);
            sysLog.setIp(ip);
            sysLog.setOperateUser(SessionUtil.getUserDetails().getUsername());
            sysLog.setCreateTime(new Date());
            sysLogDao.insertSelective(sysLog);
            logger.info("切面日志调用方法结束......");
        }  catch (Exception e) {
            //记录本地异常日志
            logger.error("==后置通知异常==");
            logger.error("异常信息:{}", e.getMessage());
        }
    }


    // 获取方法的中文备注____用于记录用户的操作日志描述
    protected String getMthodRemark(JoinPoint joinPoint)
            throws Exception {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();

        Class targetClass = Class.forName(targetName);
        Method[] method = targetClass.getMethods();
        String methode = "";
        for (Method m : method) {
            if (m.getName().equals(methodName)) {
                Class[] tmpCs = m.getParameterTypes();
                if (tmpCs.length == arguments.length) {
                    MethodLog methodCache = m.getAnnotation(MethodLog.class);
                    if (methodCache != null) {
                        methode = methodCache.remark();
                    }
                    break;
                }
            }
        }
        return methode;
    }

}
