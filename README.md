# open-boss
1. 项目后端框架采用SpringMVC+MyBits+Mysql数据库搭建
2. 项目前端页面框架使用了辉哥的H-ui页面框架，再次特别感谢在使用过程中遇到问题辉哥的细心解答
3. 项目中使用到了SpringSecurity做权限框架，实现了用户的登录验证等功能
4. 项目中提供了使用DFA算法实现的敏感词过滤类（后续会实现管理界面，进行敏感词过滤）
5. 项目目录结构说明：

        common包说明：aop 包中是使用SrpingAop实现日志记录功能，sensitivity 使用DFA算法实现敏感词过滤
        
        constant包说明：该包下存放常量类
        
        exception包说明：存放open-boss开源后台自定义异常
        
        interceptor包说明：存放使用Mybits拦截器实现的分页查询功能
        
        security包说明：存放重写SpringSecurity相关类
        
        system包说明：存放系统表相关操作，后台最基础的框架实现
        
        utils包说明：存放常用工具类
        
        resources/sql 包说明：存放该系统需要的数据库文件
